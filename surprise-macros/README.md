# Surprise!


### Requirements & Compatibility
*  The Furnace module with "Advanced Macros" toggled on.
*  Permission to run script macros.
*  DnD5e

### What it Does
Allows the DM to quickly resolve who is Surprised when a group of enemies or 
players are sneaking, hiding, ambushing, etc. DM can select which tokens to 
contest Stealth vs Passive Perception or the DM can select no tokens and the
macro will use all relevant tokens in the scene.

This macro will:
1.  Prompt the DM to select which group is being sneaky: "Enemy" group or "Player" group
2.  Get any selected player owned tokens in the scene, else if no tokens are selected it will get *all* player owned tokens in the scene
2.  Get any selected non-player tokens with the *"hostile"* disposition, else if no hostile tokens are selected it will get *all* hostile tokens in the scene
3.  Roll stealth checks for all tokens in the sneaking group
4.  Gather passive perception for the opposing group
5.  Resolve who is Surprised, accounting for the Alert feat
6.  Output the results in the chat for GMs only with the DnD 5e rules for Surprise printed below

[![Surprise! demo gif](https://i.gyazo.com/b89cb49480f563bcd98deb536d2d4fbc.gif)](https://gyazo.com/b89cb49480f563bcd98deb536d2d4fbc)

![Surprise! demo screenshot](https://i.imgur.com/UtBYyTD.png)

### Tips
1.  Only non-player tokens in the scene with the "hostile" disposition will be considered "Enemy" group
2.  Only player owned tokens with the type of "character" will be consider "Player" group